Spree::Product.class_eval do
  scope :includes_price_image, -> { includes(master: [:default_price, :images]) }
  scope :same_taxon,           -> (product) {
                                    joins(:classifications).
                                      where(spree_products_taxons: { taxon_id: product.taxons.ids }).
                                      where.not(id: product.id).
                                      distinct
                                  }
end
