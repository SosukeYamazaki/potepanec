class Potepan::ProductsController < ApplicationController
  MAX_NUMBER_RELATED_PRODUCTS = 4

  def show
    @product = Spree::Product.find(params[:id])
    @related_products = Spree::Product.
      includes_price_image.
      same_taxon(@product).
      limit(MAX_NUMBER_RELATED_PRODUCTS)
  end
end
