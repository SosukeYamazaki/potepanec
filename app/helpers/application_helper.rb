module ApplicationHelper
  # ページごとにタイトルを変更する
  def full_title(page_title = '')
    base_title = "BIGBAG Store"
    return base_title if page_title.empty?
    "#{page_title} - #{base_title}"
  end
end
