require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  let!(:category_taxonomy) { create(:taxonomy, name: 'category_taxonomy') }
  let!(:brand_taxonomy) { create(:taxonomy, name: 'brand_taxonomy') }
  let!(:taxon) { create(:taxon, taxonomy: brand_taxonomy, parent: brand_taxonomy.taxons.root) }
  let!(:products) { create_list(:product, 3, taxons: [taxon]) }

  describe "#show" do
    before do
      get :show, params: { id: taxon.id }
    end

    it "200 OK が返ってくる" do
      expect(response).to be_success
      expect(response.status).to eq(200)
    end

    it "適切なテンプレートが表示されていること" do
      expect(response).to render_template :show
    end

    it "@taxonomiesに正しく割当てられているか" do
      expect(assigns(:taxonomies)).to match_array [category_taxonomy, brand_taxonomy]
    end

    it "@taxonに正しく割当てられているか" do
      expect(assigns(:taxon)).to eq taxon
    end

    it "@productsに正しく割当てられているか" do
      expect(assigns(:products)).to match_array products
    end
  end

  describe "#show @display割当テスト" do
    it "@displayにデフォルト値が割当てられているか" do
      get :show, params: { id: taxon.id }
      expect(assigns(:display)).to eq "grid"
    end

    it "@displayにlistが割当てられているか" do
      get :show, params: { id: taxon.id, display: 'list' }
      expect(assigns(:display)).to eq "list"
    end

    it "@displayにgridが割当てられているか" do
      get :show, params: { id: taxon.id, display: 'grid' }
      expect(assigns(:display)).to eq "grid"
    end
  end
end
