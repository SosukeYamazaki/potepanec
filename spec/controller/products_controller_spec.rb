require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  let(:taxonomy) { create(:taxonomy, name: 'taxonomy') }
  let(:taxon) { create(:taxon, name: 'taxon', taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 3, taxons: [taxon]) }

  describe "showアクションテスト" do
    before do
      get :show, params: { id: product.id }
    end

    it "200 OK が返ってくるか" do
      expect(response).to be_success
      expect(response.status).to eq(200)
    end

    it "適切なテンプレートが表示されているか" do
      expect(response).to render_template :show
    end

    describe "product割当テスト" do
      it "@productに正しく割当てられているか" do
        expect(assigns(:product)).to eq product
      end
    end

    describe "related_products割当テスト" do
      it "@related_productsに正しく割当てられているか" do
        expect(assigns(:related_products)).to match_array related_products
      end

      context "関連商品が3つまでの場合" do
        it "@related_productsに3つ割当てられているか" do
          expect(assigns(:related_products).count).to eq 3
        end
      end

      context '関連商品が4つまでの場合' do
        let!(:related_product_4) { create(:product) }

        before do
          related_product_4.taxons << taxon
        end

        it '@related_productsに4つ割当てられているか' do
          expect(assigns(:related_products).count).to eq 4
        end
      end

      context '関連商品が5つまでの場合' do
        let!(:related_product_5) { create(:product) }

        before do
          related_product_5.taxons << taxon
        end

        it '@related_productsに4つ割当てられているか' do
          expect(assigns(:related_products).count).to eq 4
        end
      end
    end
  end
end
