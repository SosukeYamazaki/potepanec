require 'rails_helper'

RSpec.feature "Potepan::Products", type: :feature do
  given(:taxonomy) { create(:taxonomy, name: 'taxonomy') }
  given(:child_taxon) { create(:taxon, name: 'child_taxon', taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  given!(:top_product) { create(:product, name: 'top_product', price: 20.00, taxons: [child_taxon]) }
  given!(:related_product) { create(:product, name: 'related_product', price: 19.00, taxons: [child_taxon]) }
  given!(:no_related_product) { create(:product, name: 'no_related_product', price: 18.00, taxons: []) }

  background do
    visit potepan_product_path(top_product.id)
  end

  scenario "productsページに正しく商品が表示されているか" do
    expect(page).to have_selector '.page-title h2', text: top_product.name
    expect(page).to have_current_path(potepan_product_path(top_product.id))
    within('#media-body') do
      expect(page).to have_content top_product.name
      expect(page).to have_content top_product.display_price
    end
  end

  scenario "productsページに正しく関連商品が表示されているか" do
    expect(page).to have_current_path(potepan_product_path(top_product.id))
    within('.productBox') do
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price
      expect(page).not_to have_content no_related_product.name
      expect(page).not_to have_content no_related_product.display_price
    end
  end

  scenario "関連商品のリンクが正しく機能しているか" do
    click_on 'related_product'
    expect(page).to have_current_path(potepan_product_path(related_product.id))
    within('#media-body') do
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price
    end
  end

  scenario "一覧ページへ戻るボタンが正しく機能しているか" do
    click_on "一覧ページへ戻る"
    expect(page).to have_current_path(potepan_category_path(top_product.taxon_ids.first))
    expect(page).to have_selector '.productBox h5', text: top_product.name
    expect(page).to have_selector '.productBox h3', text: top_product.display_price
  end
end
