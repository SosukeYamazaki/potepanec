require 'rails_helper'

RSpec.feature "Potepan::Categories", type: :feature do
  given(:category_taxonomy) { create(:taxonomy, name: 'category_taxonomy') }
  given(:brand_taxonomy) { create(:taxonomy, name: 'brand_taxonomy') }
  given!(:bag_taxon) { create(:taxon, name: 'bag_taxon', taxonomy: category_taxonomy, parent_id: category_taxonomy.root.id) }
  given!(:apache_taxon) { create(:taxon, name: 'apache_taxon', taxonomy: brand_taxonomy, parent_id: brand_taxonomy.root.id) }
  given!(:bag) { create(:product, name: 'bag', price: 20.00, taxons: [bag_taxon]) }
  given!(:apache) { create(:product, name: 'apache', price: 18.00, taxons: [apache_taxon]) }

  background do
    visit potepan_category_path(bag_taxon.id)
  end

  scenario "categoriesページに正しく商品が表示されているか" do
    expect(page).to have_current_path(potepan_category_path(bag_taxon.id))
    expect(page).to have_selector '.page-title h2', text: bag_taxon.name
    within('.productBox') do
      expect(page).to have_content bag.name
      expect(page).to have_content bag.display_price
      expect(page).not_to have_content apache.name
      expect(page).not_to have_content apache.display_price
    end
  end

  scenario "grid/listの切り替えが正しくできるか" do
    click_link "List"
    expect(page).to have_current_path potepan_category_path(bag_taxon.id, display: "list")
    within('#media-body') do
      expect(page).to have_content bag.name
      expect(page).to have_content bag.display_price
      expect(page).to have_content bag.description
    end
    click_link "Grid"
    expect(page).to have_current_path potepan_category_path(bag_taxon.id, display: "grid")
    within('.productBox') do
      expect(page).to have_content bag.name
      expect(page).to have_content bag.display_price
      expect(page).not_to have_content bag.description
    end
  end

  scenario "productのリンクが正しく機能しているか" do
    expect(page).to have_current_path(potepan_category_path(bag_taxon.id))
    click_link bag.name
    expect(page).to have_current_path(potepan_product_path(bag.id))
    expect(page).to have_selector '.page-title h2', text: bag.name
    within('#media-body') do
      expect(page).to have_content bag.name
      expect(page).to have_content bag.display_price
    end
  end

  scenario "category/brandメニューが正しく表示されているか" do
    within('.navbar-side-collapse') do
      expect(page).to have_link category_taxonomy.root.name
      expect(page).to have_link "bag_taxon"
      expect(page).to have_link brand_taxonomy.root.name
      expect(page).to have_link "apache_taxon"
    end
  end
end
