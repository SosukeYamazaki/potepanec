require 'rails_helper'

describe "Spree::Product", type: :model do
  let(:taxonomy) { create(:taxonomy, name: 'taxonomy') }
  let(:taxon) { create(:taxon, name: 'taxon', taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:top_product) { create(:product, name: 'top_product', taxons: [taxon]) }
  let!(:related_product) { create(:product, name: 'related_product', taxons: [taxon]) }
  let!(:no_related_product) { create(:product, name: 'no_related_product', taxons: []) }

  describe "scopes#same_taxon(where.not)" do
    it "関連商品を取得できているか" do
      expect(Spree::Product.same_taxon(top_product)).to contain_exactly(related_product)
    end

    it "同じ商品は除外出来ているか" do
      expect(Spree::Product.same_taxon(top_product)).not_to include(top_product)
    end

    it "関連商品ではないものは除外出来ているか" do
      expect(Spree::Product.same_taxon(top_product)).not_to include(no_related_product)
    end
  end
end
